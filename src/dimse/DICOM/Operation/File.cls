Class dimse.DICOM.Operation.File Extends Ens.BusinessOperation
{

/// This is the directory where the local files will be stored
Parameter SETTINGS = "FileStorageDirectory";

/// This is the directory where the incoming DICOM files will be stored
Property FileStorageDirectory As %String(MAXLEN = "") [ InitialExpression = "/" ];

/// This is the default message handler.  All request types not declared in the message map are delivered here
Method OnMessage(pRequest As %Library.Persistent, Output pResponse As %Library.Persistent) As %Status
{
	#dim tSC As %Status = $$$OK
	#dim e As %Exception.AbstractException
	#dim tFile As EnsLib.DICOM.File
	#dim tFileName As %String
	try {
		
		#; We should only ever see DICOM Documents here
		$$$ASSERT(pRequest.%Extends("EnsLib.DICOM.Document"))
		$$$LOGINFO(pRequest.GetValueAt("DataSet.SOPInstanceUID"))
		Set tStudyInstanceUID = pRequest.GetValueAt("DataSet.SOPInstanceUID")
		#; Create a DICOM File from the DICOM document
		Set tSC=##class(EnsLib.DICOM.File).CreateFromDocument(pRequest,.tFile)
		If $$$ISERR(tSC) Quit
		Set tSC = ##class(%Library.File).CreateDirectory(..FileStorageDirectory_"/"_tStudyInstanceUID)
		#; Create a unique filename
		Set tFileName=..NewFilename(..FileStorageDirectory_"/"_tStudyInstanceUID)

		$$$LOGINFO(tFileName)
		#; Create a new file with a unique name and the dcm extension in the target directory
		Set tSC=tFile.Save(tFileName)

		#; Use pydicom to convert to jpeg
		//Set tResponse = ..SaveDicomAsJPEG(tFileName)
		//$$$LOGINFO("Conversion Response: "_tResponse)

		#; convert to fhir
		Set tResponse = ..Dicom2Fhir(..FileStorageDirectory_"/"_tStudyInstanceUID)
		$$$LOGINFO("Conversion Response: "_tResponse)
		
	} catch(e) {
		Set tSC=e.AsStatus()
	}
	Quit tSC
}

/// Create a new file name within the specified directory
ClassMethod NewFilename(dir) As %String [ CodeMode = expression ]
{
##class(%File).NormalizeDirectory(dir)_(##class(%FileBinaryStream).NewFileName("dcm"))
}

ClassMethod SaveDicomAsJPEG(image) [ Language = python ]
{
	import pydicom as dicom
	import os
	import cv2
	import PIL # optional
	# make it True if you want in PNG format
	PNG = False
	jpg_folder_path = "/opt/irisisbuild/input/converted/"

	ds = dicom.dcmread(image)
	pixel_array_numpy = ds.pixel_array
	if PNG == False:
		image = image.replace('.dcm', '.jpg')
	else:
		image = image.replace('.dcm', '.png')
	result = cv2.imwrite(os.path.join(jpg_folder_path, image), pixel_array_numpy)

	return result
}

ClassMethod Dicom2Fhir(pStudyDir) [ Language = python ]
{
	import os
	import dicom2fhir
	from fhir import resources as fr

    # process_dicom_2_fhir
	gar = dicom2fhir.main.process_dicom_2_fhir(pStudyDir)
	print(gar)
}

}
