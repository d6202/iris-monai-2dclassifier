
## DIMSE Iris Monai 2dclassifier Service 
This is an application to classify dicom medical images with Monai and InterSystems IRIS Embedded Python.  The example 2d classification example from Monai executed in an IRIS production.  

## Installation
1. Clone/git pull the repo into any local directory

```
$ git clone https://github.com/dimse-cloud/iris-monai-2dclassifier.git
```

2. Open a Docker terminal in this directory and run:

```
$ docker-compose build
```

3. Run the IRIS container:

```
$ docker-compose up -d 
```

# Credits
This application uses the Monai framework
