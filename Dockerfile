#FROM intersystemsdc/iris-community
FROM store/intersystems/irishealth-community:2021.2.0.649.0

USER root

ENV DEBIAN_FRONTEND noninteractive


# install libraries required ImageAI/OpenCV to process images and videos
RUN apt-get -y update \
    && apt-get -y install apt-utils \
    && apt-get install -y build-essential unzip pkg-config \
        zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev \
        libssl-dev libreadline-dev libffi-dev wget \
    && apt-get install -y ffmpeg libsm6 libxext6 \
    && apt-get install -y python3-pip   

# use pip3 (the python zpm) to install imageai and the imageai dependencies
RUN pip3 install --upgrade pip setuptools~=50.3.2 wheel
RUN pip3 install --target /usr/irissys/mgr/python pydicom==2.0.0 keras==2.4.3 numpy==1.19.3 pillow==8.1.1 scipy==1.4.1 h5py==2.10.0 matplotlib==3.3.2 opencv-python keras-resnet==0.2.0 tqdm
# RUN pip3 install --target /usr/irissys/mgr/python monai-weekly
RUN pip3 install --target /usr/irissys/mgr/python fhir.resources==5.1.1

USER root   
WORKDIR /opt/irisbuild
RUN chown ${ISC_PACKAGE_MGRUSER}:${ISC_PACKAGE_IRISGROUP} /opt/irisbuild
USER ${ISC_PACKAGE_MGRUSER}

WORKDIR /opt/irisbuild
COPY python python
COPY input input
COPY output output
COPY models models
COPY src src
COPY module.xml module.xml
COPY iris.script iris.script
COPY wheels wheels

RUN pip3 install --target /usr/irissys/mgr/python wheels/dicom2fhir-0.0.8-py3-none-any.whl 

# download the trained model used to detect objects and persons inside images
# ADD https://github.com/OlafenwaMoses/ImageAI/releases/download/essentials-v5/resnet50_coco_best_v2.1.0.h5 models

USER root
RUN chmod -R 777 input
RUN chmod -R 777 output
RUN chmod -R 777 models


USER ${ISC_PACKAGE_MGRUSER}

RUN iris start IRIS \
	&& iris session IRIS < iris.script \
    && iris stop IRIS quietly
